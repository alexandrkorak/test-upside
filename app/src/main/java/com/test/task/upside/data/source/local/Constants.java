package com.test.task.upside.data.source.local;

public interface Constants {
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
}
