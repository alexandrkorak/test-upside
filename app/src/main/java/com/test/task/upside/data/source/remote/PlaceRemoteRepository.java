package com.test.task.upside.data.source.remote;

import androidx.annotation.NonNull;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.tasks.geocode.GeocodeParameters;
import com.esri.arcgisruntime.tasks.geocode.GeocodeResult;
import com.esri.arcgisruntime.tasks.geocode.LocatorTask;
import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PlaceRemoteRepository implements PlaceRemoteDataSource {
    @NonNull
    private final LocatorTask locator = new LocatorTask(Constants.URL);

    @Override
    @NonNull
    public List<GeocodeResult> getPlaces(@NonNull LatLng latLng) {
        GeocodeParameters parameters = new GeocodeParameters();
        Point searchPoint = new Point(latLng.longitude, latLng.latitude);

        parameters.setPreferredSearchLocation(searchPoint);
        parameters.setMaxResults(Constants.MAX_PLACES_AMOUNT);

        List<String> outputAttributes = parameters.getResultAttributeNames();
        outputAttributes.add(Constants.PLACE_ADDR);
        outputAttributes.add(Constants.PLACE_NAME);
        final ListenableFuture<List<GeocodeResult>> results = locator.geocodeAsync(Constants.FOOD_PLACE, parameters);
        try {
            return results.get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
        return Collections.emptyList();
    }
}
