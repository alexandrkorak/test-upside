package com.test.task.upside.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Optional<V> {
    @Nullable
    private final V value;

    private Optional(@Nullable V value) {
        this.value = value;
    }

    public static <T> Optional<T> fromNullable(@Nullable T value) {
        return new Optional<>(value);
    }

    public static <T> Optional<T> of(@NonNull T t) {
        if (t == null) {
            throw new IllegalArgumentException("Should not be null");
        }
        return new Optional<>(t);
    }

    public boolean isPresent() {
        return value != null;
    }

    @NonNull
    public V get() {
        if (value == null) {
            throw new IllegalStateException();
        }
        return value;
    }
}
