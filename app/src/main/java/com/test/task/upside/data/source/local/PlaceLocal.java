package com.test.task.upside.data.source.local;

import androidx.annotation.NonNull;

import com.test.task.upside.data.place.Place;

import io.realm.RealmObject;


public class PlaceLocal extends RealmObject {
    private double latitude;
    private double longitude;
    private String title;
    private String address;
    private String name;

    public PlaceLocal() {

    }

    public PlaceLocal(@NonNull Place place) {
        latitude = place.latLng.latitude;
        longitude = place.latLng.longitude;
        title = place.title;
        address = place.address;
        name = place.placeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
