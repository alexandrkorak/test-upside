package com.test.task.upside.data.source.remote;

public interface Constants {
    int MAX_PLACES_AMOUNT = 20;
    String URL = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer";
    String FOOD_PLACE = "Food";
    String PLACE_ADDR = "Place_addr";
    String PLACE_NAME = "PlaceName";
}
