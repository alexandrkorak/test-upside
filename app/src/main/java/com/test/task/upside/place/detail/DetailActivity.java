package com.test.task.upside.place.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.test.task.upside.R;
import com.test.task.upside.data.place.Place;

public class DetailActivity extends AppCompatActivity {
    private static final String PLACE_PARAM = "PLACE";

    public static void start(@NonNull Context context, @NonNull Place place) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(PLACE_PARAM, place);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Place place = getIntent().getExtras().getParcelable(PLACE_PARAM);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(place.title);

        TextView nameView = findViewById(R.id.name);
        nameView.setText(place.placeName);
        TextView addressView = findViewById(R.id.address);
        addressView.setText(place.address);
        TextView coordinatesView = findViewById(R.id.coordinates);
        coordinatesView.setText(getString(R.string.coordinates, place.latLng.latitude, place.latLng.longitude));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
