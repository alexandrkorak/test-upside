package com.test.task.upside.data;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.google.android.gms.maps.model.LatLng;
import com.test.task.upside.data.place.Place;
import com.test.task.upside.data.place.PlaceMapper;
import com.test.task.upside.data.source.local.PlaceLocalDataSource;
import com.test.task.upside.data.source.local.PlaceLocalRepository;
import com.test.task.upside.data.source.remote.Constants;
import com.test.task.upside.data.source.remote.PlaceRemoteDataSource;
import com.test.task.upside.data.source.remote.PlaceRemoteRepository;

import java.util.List;

public class PlaceRepository implements PlaceDataSource {
    private static final double DELTA = .0175;
    @NonNull
    private final PlaceMapper placeMapper = new PlaceMapper();
    @NonNull
    private final PlaceLocalDataSource placeLocalDataSource = new PlaceLocalRepository();
    @NonNull
    private final PlaceRemoteDataSource placeRemoteDataSource = new PlaceRemoteRepository();

    @Override
    @WorkerThread
    @NonNull
    public List<Place> getPlaces(@NonNull LatLng latLng) {
        List<Place> places = placeMapper.fromLocal(
                placeLocalDataSource.getPlaces(latLng.latitude - DELTA, latLng.longitude - DELTA,
                        latLng.latitude + DELTA, latLng.longitude + DELTA));

        if (places.size() < Constants.MAX_PLACES_AMOUNT) {
            places = placeMapper.fromRemote(placeRemoteDataSource.getPlaces(latLng));
            placeLocalDataSource.clear();
            placeLocalDataSource.setPlaces(placeMapper.from(places));
        }
        return places;
    }
}
