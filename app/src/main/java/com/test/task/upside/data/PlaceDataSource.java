package com.test.task.upside.data;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.google.android.gms.maps.model.LatLng;
import com.test.task.upside.data.place.Place;

import java.util.List;

public interface PlaceDataSource {
    @WorkerThread
    List<Place> getPlaces(@NonNull LatLng latLng);
}
