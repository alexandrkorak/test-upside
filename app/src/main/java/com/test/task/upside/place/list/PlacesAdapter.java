package com.test.task.upside.place.list;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.test.task.upside.R;
import com.test.task.upside.data.place.Place;

import java.util.ArrayList;
import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.PlaceViewHolder> {
    @NonNull
    private final List<Place> places;
    @NonNull
    private final Listener listener;

    PlacesAdapter(@NonNull List<Place> places, @NonNull Listener listener) {
        this.places = new ArrayList<>(places);
        this.listener = listener;
    }

    @NonNull
    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.place_item, parent, false);
        return new PlaceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder holder, int position) {
        Place place = places.get(position);
        holder.itemView.setOnClickListener(v -> listener.onClick(place));
        holder.titleView.setText(place.title);
        holder.coordinatesView.setText(
                holder.itemView.getContext().getString(R.string.coordinates, place.latLng.latitude, place.latLng.longitude));
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public interface Listener {
        void onClick(@NonNull Place place);
    }

    static class PlaceViewHolder extends RecyclerView.ViewHolder {
        TextView titleView;
        TextView coordinatesView;

        PlaceViewHolder(@NonNull ConstraintLayout cardView) {
            super(cardView);
            titleView = cardView.findViewById(R.id.title);
            coordinatesView = cardView.findViewById(R.id.coordinates);
        }
    }
}
