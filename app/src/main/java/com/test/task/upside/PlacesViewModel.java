package com.test.task.upside;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.test.task.upside.data.Optional;
import com.test.task.upside.data.PlaceDataSource;
import com.test.task.upside.data.PlaceRepository;
import com.test.task.upside.data.place.Place;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class PlacesViewModel extends ViewModel {
    private static final long DELAY = 300;
    @NonNull
    private static final PublishSubject<Optional<LatLng>> subject = PublishSubject.create();
    @NonNull
    private final PlaceDataSource placeDataSource = new PlaceRepository();
    @NonNull
    private final Disposable disposable;
    @NonNull
    private MutableLiveData<List<Place>> places = new MutableLiveData<>();

    public PlacesViewModel() {
        places.postValue(Collections.emptyList());
        disposable = subject
                .debounce(locOptional -> locOptional.isPresent() ? Observable.timer(DELAY, TimeUnit.MILLISECONDS) : Observable.empty())
                .distinctUntilChanged()
                .switchMap(locOptional -> locOptional.isPresent() ? Observable.create((ObservableOnSubscribe<List<Place>>) emitter -> {
                    LatLng latLng = locOptional.get();
                    List<Place> fetchedPlaces = placeDataSource.getPlaces(latLng);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(fetchedPlaces);
                    }
                }).onErrorResumeNext(Observable.empty()).subscribeOn(Schedulers.io()) : Observable.empty())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(places::postValue);
    }

    @NonNull
    LiveData<List<Place>> getPlaces() {
        return places;
    }

    void stop() {
        subject.onNext(Optional.fromNullable(null));
    }

    void fetch(@NonNull LatLng latLng) {
        subject.onNext(Optional.of(latLng));
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
    }
}
