package com.test.task.upside.data.source.local;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import java.util.List;

public interface PlaceLocalDataSource {
    @WorkerThread
    @NonNull
    List<PlaceLocal> getPlaces(
            double latitudeFrom, double longitudeFrom,
            double latitudeTo, double longitudeTo
    );

    @WorkerThread
    void clear();

    @WorkerThread
    void setPlaces(@NonNull List<PlaceLocal> placesLocal);
}
