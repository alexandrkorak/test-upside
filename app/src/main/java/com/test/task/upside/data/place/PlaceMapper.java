package com.test.task.upside.data.place;

import androidx.annotation.NonNull;

import com.esri.arcgisruntime.tasks.geocode.GeocodeResult;
import com.google.android.gms.maps.model.LatLng;
import com.test.task.upside.data.source.local.PlaceLocal;
import com.test.task.upside.data.source.remote.Constants;

import java.util.ArrayList;
import java.util.List;

public class PlaceMapper {
    @NonNull
    public PlaceLocal from(@NonNull Place place) {
        PlaceLocal placeLocal = new PlaceLocal();
        placeLocal.setLatitude(place.latLng.latitude);
        placeLocal.setLongitude(place.latLng.longitude);
        placeLocal.setTitle(place.title);
        placeLocal.setAddress(place.address);
        placeLocal.setName(place.placeName);
        return placeLocal;
    }

    @NonNull
    public List<PlaceLocal> from(@NonNull List<Place> places) {
        List<PlaceLocal> localPlaces = new ArrayList<>();
        for (Place place : places) {
            localPlaces.add(from(place));
        }
        return localPlaces;
    }

    @NonNull
    public Place fromLocal(@NonNull PlaceLocal placeLocal) {
        return new Place(
                new LatLng(placeLocal.getLatitude(), placeLocal.getLongitude()),
                placeLocal.getTitle(),
                placeLocal.getAddress(),
                placeLocal.getName());
    }

    @NonNull
    public List<Place> fromLocal(@NonNull List<PlaceLocal> localPlaces) {
        List<Place> places = new ArrayList<>();
        for (PlaceLocal localPlace : localPlaces) {
            places.add(fromLocal(localPlace));
        }
        return places;
    }

    @NonNull
    public Place fromRemote(@NonNull GeocodeResult geocodeResult) {
        return new Place(
                new LatLng(geocodeResult.getDisplayLocation().getY(), geocodeResult.getDisplayLocation().getX()),
                geocodeResult.getLabel(),
                (String) geocodeResult.getAttributes().get(Constants.PLACE_ADDR),
                (String) geocodeResult.getAttributes().get(Constants.PLACE_NAME));
    }

    @NonNull
    public List<Place> fromRemote(@NonNull List<GeocodeResult> geocodeResults) {
        List<Place> places = new ArrayList<>();
        for (GeocodeResult geocodeResult : geocodeResults) {
            places.add(fromRemote(geocodeResult));
        }
        return places;
    }
}
