package com.test.task.upside.data.source.local;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class PlaceLocalRepository implements PlaceLocalDataSource {
    @Override
    @WorkerThread
    @NonNull
    public List<PlaceLocal> getPlaces(
            double latitudeFrom, double longitudeFrom,
            double latitudeTo, double longitudeTo
    ) {
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<PlaceLocal> realmResults = realm.where(PlaceLocal.class)
                    .between(Constants.LATITUDE, latitudeFrom, latitudeTo)
                    .and()
                    .between(Constants.LONGITUDE, longitudeFrom, longitudeTo)
                    .findAll();
            return realm.copyFromRealm(realmResults);
        }
    }

    @Override
    public void clear() {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            realm.delete(PlaceLocal.class);
            realm.commitTransaction();
        }
    }

    @Override
    @WorkerThread
    public void setPlaces(@NonNull List<PlaceLocal> placesLocal) {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            realm.insert(placesLocal);
            realm.commitTransaction();
        }
    }
}
