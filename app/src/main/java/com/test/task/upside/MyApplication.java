package com.test.task.upside;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import io.realm.Realm;

public class MyApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        Realm.init(this);
    }
}
