package com.test.task.upside.data.source.remote;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.esri.arcgisruntime.tasks.geocode.GeocodeResult;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface PlaceRemoteDataSource {
    @NonNull
    @WorkerThread
    List<GeocodeResult> getPlaces(@NonNull LatLng latLng);
}
