package com.test.task.upside;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.test.task.upside.data.place.Place;
import com.test.task.upside.place.detail.DetailActivity;
import com.test.task.upside.place.list.ListActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0x12;
    private static final int DEFAULT_ZOOM = 14;
    @NonNull
    private static final String LAST_KNOWN_LOCATION_PARAM = "LAST_KNOWN_LOCATION";
    private static final double DUMMY_LATITUDE = 37.421998189179185;
    private static final double DUMMY_LONGITUDE = -122.0839998498559;
    @NonNull
    private final Map<String, Place> map = new HashMap<>();
    @NonNull
    private final LatLng mDefaultLocation = new LatLng(DUMMY_LATITUDE, DUMMY_LONGITUDE);
    private PlacesViewModel viewModel;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    @Nullable
    private LatLng mLastKnownLocation;
    private View overlay;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        overlay = findViewById(R.id.overlay);
        progressBar = findViewById(R.id.progressBar);

        mLastKnownLocation = savedInstanceState == null ? null : savedInstanceState.getParcelable(LAST_KNOWN_LOCATION_PARAM);
        viewModel = ViewModelProviders.of(this).get(PlacesViewModel.class);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (!checkHasPermission()) {
            requestPermission();
        } else {
            processLocation();
        }
    }

    private boolean checkHasPermission() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void hideProgressIndicator() {
        progressBar.setVisibility(View.GONE);
        overlay.setVisibility(View.GONE);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(LAST_KNOWN_LOCATION_PARAM, mLastKnownLocation);
        super.onSaveInstanceState(outState);
    }

    private void displayPlaces(@NonNull List<Place> places) {
        if (mMap != null) {
            mMap.clear();
            map.clear();
            for (Place place : places) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title(place.title);
                markerOptions.position(place.latLng);
                map.put(mMap.addMarker(markerOptions).getId(), place);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            processLocation();
        } else {
            finish();
        }
    }

    @RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
    private void getDeviceLocation() {
        Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
        locationResult.addOnCompleteListener(this, task -> {
            mLastKnownLocation = task.isSuccessful() && task.getResult() != null ? new LatLng(task.getResult().getLatitude(), task.getResult().getLongitude()) : mDefaultLocation;
            viewModel.getPlaces().observe(this, this::displayPlaces);
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLastKnownLocation, DEFAULT_ZOOM));
                findViewById(R.id.all).setOnClickListener(v -> ListActivity.start(MainActivity.this, viewModel.getPlaces().getValue()));
                hideProgressIndicator();
            }
        });
    }

    private void processCameraIdle() {
        if (mLastKnownLocation != null) {
            CameraPosition cameraPosition = mMap.getCameraPosition();
            mLastKnownLocation = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
            viewModel.fetch(cameraPosition.target);
        }
    }

    private void processInfoWindowClick(@NonNull Marker marker) {
        Place place = map.get(marker.getId());
        if (place != null) {
            DetailActivity.start(this, place);
        }
    }

    @RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
    private void processLocation() {
        if (mLastKnownLocation != null) {
            viewModel.getPlaces().observe(this, this::displayPlaces);
            findViewById(R.id.all).setOnClickListener(v -> ListActivity.start(MainActivity.this, viewModel.getPlaces().getValue()));
            hideProgressIndicator();
        } else {
            getDeviceLocation();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraMoveStartedListener(i -> viewModel.stop());
        mMap.setOnCameraIdleListener(this::processCameraIdle);
        mMap.setOnMarkerClickListener(marker -> {
            marker.showInfoWindow();
            return true;
        });
        mMap.setOnInfoWindowClickListener(this::processInfoWindowClick);

        if (checkHasPermission()) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            processLocation();
        }
    }
}
