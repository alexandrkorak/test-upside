package com.test.task.upside.data.place;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

public class Place implements Parcelable {
    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
    @NonNull
    public final LatLng latLng;
    @NonNull
    public final String title;
    @NonNull
    public final String address;
    @NonNull
    public final String placeName;

    public Place(
            @NonNull LatLng latLng, @NonNull String title,
            @NonNull String address, @NonNull String placeName) {
        this.latLng = latLng;
        this.title = title;
        this.address = address;
        this.placeName = placeName;
    }

    protected Place(Parcel in) {
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        title = in.readString();
        address = in.readString();
        placeName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(latLng, flags);
        dest.writeString(title);
        dest.writeString(address);
        dest.writeString(placeName);
    }
}
